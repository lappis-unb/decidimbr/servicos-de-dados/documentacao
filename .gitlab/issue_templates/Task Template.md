## Descrição da Task

<!-- Uma breve descrição do problema ou tarefa. -->

## Possivel Solução

<!-- Uma possivel solução do problema -->

## Critérios de Aceitação

- [ ] Critério 1
- [ ] Critério 2
- [ ] Critério 3

## Anexos

<!-- Inserir capturas de tela, se aplicável] -->

<!-- Quick Actions (NAO APAGAR!) -->
<!-- Mais informacoes em: https://docs.gitlab.com/ee/user/project/quick_actions.html -->

/label ~"Serviços de Dados"
/iteration [cadence:1046389] --current
/weight 1
/due next Thursday
