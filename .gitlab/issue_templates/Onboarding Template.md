# **ONBOARDING**

Seja bem-vindo ao projeto. Essa issue é para que você esteja a par das informações antes de começar com as atividades referentes ao desenvolvimento.

## **Lista de atividades**

- [ ] [Wiki](https://gitlab.com/groups/lappis-unb/decidimbr/servicos-de-dados/-/wikis/home)
- [ ] [Ler guia de contribuição](https://gitlab.com/lappis-unb/decidimbr/servicos-de-dados/airflow-dags/-/blob/development/CONTRIBUTING.md?ref_type=heads)
- [ ] [Readme do repositório de DAGs](https://gitlab.com/lappis-unb/decidimbr/servicos-de-dados/airflow-dags/-/blob/development/README.md?ref_type=heads)
- [ ] [Ler a documentação do time](https://documentacao-lappis-unb-decidimbr-servicos-de-da-1b875c3508a3df.gitlab.io/docs/intro)
- [ ] [Adicionar o nome no codeowner](https://gitlab.com/lappis-unb/decidimbr/servicos-de-dados/airflow-dags/-/blob/development/CODEOWNERS?ref_type=heads)
- [ ]  Solicitar ao time o acesso aos repositórios do Gitlab.
 Solicitar ao time o acesso ao FIGMA.
- [ ] Providenciar email e nome de usuário ao time de Infraestutura para liberar os acessos de VPN e contas de homologação.
- [ ] [Configurar as chaves SSH na máquina local](https://docs.gitlab.com/ee/user/ssh.html)
- [ ] [Instalar e executar o ambiente na máquina local.](https://gitlab.com/lappis-unb/decidimbr/servicos-de-dados/airflow-docker)
- [ ] Atualizar a documentação:
   - [ ] Remover instruções redundantes sobre a configuração do ambiente ou quaisquer aspectos
   - [ ] Atualizar links quebrados
   - [ ] Corrigir quaisquer aspectos que precisar

## **Leitura complementar**

1. [Método de acolhimento](https://gitlab.com/lappis-unb/decidimbr/ecossistemasl/-/wikis/metodologia/M%C3%A9todo-de-acolhimento)
2. [Diagram do Método de acolhimento no FIGMA](https://www.figma.com/file/cgjQw1jpC01LKIYjKLAkM4/M%C3%A9todo-de-Acolhimento---MA?type=whiteboard&node-id=0-1)
