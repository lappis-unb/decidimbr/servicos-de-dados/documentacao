import os
from pathlib import Path
import sys

def generate_file_documentation(source_dir, docs_base_dir='docs'):
    """
    Recursively scan a directory for Python files and create corresponding markdown documentation files.
    
    Args:
        source_dir (str): Root directory to start searching for Python files
        docs_base_dir (str, optional): Base directory for generated documentation. Defaults to 'docs'.
    """
    # Ensure the docs base directory exists
    os.makedirs(docs_base_dir, exist_ok=True)
    
    docs_base_dir = Path(docs_base_dir)
    
    # Walk through all directories and files in the source directory
    for root, dirs, files in os.walk(source_dir):
        for file in files:

            if file.endswith('.py'):
                # Construct full path to the source file
                full_source_path = Path(root).joinpath(file)
                if full_source_path.as_posix().startswith("airflow-dags/dags/dbt/") \
                or full_source_path.as_posix().startswith("airflow-dags/tests/") \
                or full_source_path.name.startswith("__init__"):
                    continue

                new_path = Path("/".join(full_source_path.parts[1::]))
                root_folder = full_source_path.parts[0].replace("-", " ").title()

                doc_path = docs_base_dir.joinpath(f"{root_folder}" / new_path).with_name(new_path.name.replace(".py", ".md"))

                doc_path.parent.mkdir(parents=True, exist_ok=True)

                doc_reference_path = new_path.as_posix().replace("/", ".").replace('.py', '')
                with open(doc_path, 'w') as doc_file:
                    doc_file.write(f"::: {doc_reference_path}\n")

                
                print(f"Generated documentation for {full_source_path} at {doc_path}")

def main():
    # Check if source directory is provided as an argument
    if len(sys.argv) < 2:
        print("Usage: python script.py <source_directory> [docs_directory]")
        sys.exit(1)
    
    source_dir = sys.argv[1]
    docs_dir = sys.argv[2] if len(sys.argv) > 2 else 'docs'
    
    # Validate source directory
    if not os.path.isdir(source_dir):
        print(f"Error: {source_dir} is not a valid directory")
        sys.exit(1)
    
    # Run the documentation generation
    generate_file_documentation(source_dir, docs_dir)
    print("Documentation generation complete.")

if __name__ == "__main__":
    main()