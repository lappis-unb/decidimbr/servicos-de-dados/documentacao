FROM python:3.12-alpine3.20

RUN apk add --no-cache git bash nano

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY assets assets

COPY mkdocs.yaml mkdocs.yaml
COPY gen_docs.py gen_docs.py

COPY docs docs
# Clones the repo that will have the doc generated
RUN git clone https://gitlab.com/lappis-unb/decidimbr/servicos-de-dados/airflow-dags.git

RUN python3 gen_docs.py airflow-dags/

ENTRYPOINT [ "mkdocs", "serve", "-w", "airflow-dags" ]
